import os
from pytz import timezone
import logging


class Config(object):
    # Flask Settings
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///auth.db'
    SECRET_KEY = r'%\xe8\x8e\x90\xa7\x1a{V\x02\x9cd)q^# \x93\xbd\xb6B\xc0\x86x\xd8'
    USERNAME = 'admin'
    PASSWORD = 'admin'
    POS_ROWS = 5
    OTHER_DEVICE_ROWS = 9
    MONGODB_URI = 'mongodb://project-dashboard:{}' \
                  '@compassfss.com:27017/fss_implementations' \
                  '?authSource=auth_db' \
        .format(os.getenv('FLASK_MONGODB_PASS'))
    FP_DEVICE_ORDER_SUFFIX = 'FreedomPay_Device_Order_Form.pdf'
    TEMP_FOLDER = os.getenv('FLASK_TEMP')
    EASTERN = timezone('US/Eastern')
    FAVICON_URI = 'img/favicon.ico'

    # Flask-User settings
    USER_APP_NAME = "Compass FSS Project Tracker" # Used by email templates


class ProductionConfig(Config):
    CONFIG_TYPE = 'PROD'
    MONGODB_URI = MONGODB_URI = 'mongodb://project-dashboard:{}' \
                                '@localhost:27017/fss_implementations' \
                                '?authSource=auth_db' \
        .format(os.getenv('FLASK_MONGODB_PASS'))
    FAVICON_URI = 'img/compass_favicon.ico'

    # Flask-Mail settings
    MAIL_SERVER = 'localhost'
    MAIL_PORT = 587
    MAIL_DEFAULT_SENDER = 'support@compassfss.com'


class DevelopmentConfig(Config):
    CONFIG_TYPE = 'DEV'
    DEBUG = True
    LOG_LEVEL = logging.DEBUG
    FAVICON_URI = 'img/test_tube.ico'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///dev_auth.db'

    # Flask-Mail settings
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASS")
    MAIL_DEFAULT_SENDER = MAIL_USERNAME
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = '465'
    MAIL_USE_SSL = True
    USER_ENABLE_CONFIRM_EMAIL = False
    USER_ENABLE_LOGIN_WITHOUT_CONFIRM = True


class TestingConfig(Config):
    CONFIG_TYPE = 'TEST'
    MONGODB_URI = 'mongodb://project-dashboard:{}' \
                  '@compassfss.com:27017/test_db' \
                  '?authSource=auth_db' \
        .format(os.getenv('FLASK_MONGODB_PASS'))
    TESTING = True
