#!python3

from pymongo import MongoClient
from bson.json_util import dumps
from pprint import pprint
from utils import *
import ssl

mongodb_uri = 'mongodb://project-dashboard:x9cd)q^#' \
              '@server.fssdash.xyz:27017/fss_implementations' \
              '?authSource=auth_db'
mongobv_uri = 'mongodb://project-dashboard:x9cd)q^#' \
              '@bvuschd2501vm.na.compassgroup.corp:27017/fss_implementations' \
              '?authSource=auth_db&ssl=true&ssl_cert_reqs=CERT_NONE'
mongolab_uri = "mongodb://heroku_4hdsl8wq:e0i4uimvgji0jihtvun5s4je12@ds013941.mlab.com:13941/heroku_4hdsl8wq"
hero_client = MongoClient(mongolab_uri,
                          connectTimeoutMS=30000,
                          socketTimeoutMS=None,
                          socketKeepAlive=True)
bv_client = MongoClient(mongobv_uri)
client = MongoClient(mongodb_uri)
HERO_DB = hero_client.get_default_database()
BV_DB = bv_client.get_default_database()
db = DB = client.get_default_database()

unit_collection = DB.unit_info

NOT_STARTED = 0
ORDERED = 1
GATHERING_REQ = 2
INSTALLED = 2
SHIPPING_CONFIRMED = 2
COMPLETED = 3
N_A = 3


def add_group_to_network(group, network):
    for key in group:
        if key in network:
            network[key] += group[key]
        else:
            network[key] = group[key]
    return network


def get_location_qty(info):
    if "qty" in info and "location" in info:
        return {info["location"]: int(info["qty"])}
    elif "qty" in info:
        return {"unknown_location": int(info["qty"])}
    else:
        return False


def get_network_groups(record):
    network_groups = {}
    if type(record) is dict:
        for _, item in record.items():
            network_groups = get_additions(item, network_groups)
    elif type(record) is list:
        for item in record:
            network_groups = get_additions(item, network_groups)
    return network_groups


def get_additions(item, network_groups):
    additions = get_location_qty(item)
    if additions:
        network_groups = add_group_to_network(additions, network_groups)
    elif type(item) is dict or type(item) is list:
        network_groups = add_group_to_network(get_network_groups(item), network_groups)
    return network_groups


def print_unit_info(unit_record):
    print(dumps(unit_record, sort_keys=True, indent=4))


def get_devices_by_location(record, locations):
    location_record = {}
    for location in locations:
        device_record = []
        for device_type, devices in record["network_devices"].items():
            for device in devices:
                if device['location'] == location:
                    if device_type == 'other':
                        device_count = {device['type']: device['qty']}
                    else:
                        device_count = {device_type: device['qty']}
                    device_record.append(device_count)
        location_record[location] = device_record
    return location_record


def get_all(myjson, key):
    if type(myjson) == str:
        myjson = json.loads(myjson)
    if type(myjson) is dict:
        for jsonkey in myjson:
            if type(myjson[jsonkey]) in (list, dict):
                get_all(myjson[jsonkey], key)
            elif jsonkey == key:
                print(myjson[jsonkey])
    elif type(myjson) is list:
        for item in myjson:
            if type(item) in (list, dict):
                get_all(item, key)


def lookup_unit(unit_number, db=None):
    db = db or DB
    unit = db.unit_info.find_one({'unit_number': unit_number})
    return unit


def display_unit(unit):
    if isinstance(unit, dict):
        pprint(unit, indent=2)
    else:
        pprint(lookup_unit(unit))


def update_unit(unit, key, value, db=None):
    if isinstance(unit, dict):
        unit_number = str(unit.get('unit_number'))
    else:
        unit_number = str(unit)
    db = db or DB
    result = db.unit_info.update_one(
        {'unit_number': unit_number}, {
            '$set': {
                key: value
            }
        }
    )
    return result
