var unitIsOnSharepoint;
var test_data;
var completedStatuses = ['NA', 'Complete', 'Completed', 'Client Installed', 'Received', 'Client Provided'];
var numberOfUpdates = 0;
var flashMessageTimeout;

//collapse navbar when it loses focus
$(document).ready(function() {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var $navbar = $(".navbar-collapse");
        var _opened = $navbar.hasClass("in");
        if (_opened === true && !clickover.hasClass("navbar-toggle") && clickover.parents('.navbar-collapse').length < 1) {
            $navbar.collapse('hide');
        }
    });
});

function hideFields() {
    $('#tblPosTerminals > tbody > tr').each(function(index) {
        if (index > 1) {
            $(this).hide();
        }
    });
    $('#tblOtherNetworkDevices > tbody > tr').each(function(index) {
        if (index > 1) {
            $(this).hide();
        }
    });

    $('#sharepoint_add_div').hide();
    removeFeedback('sp_check_icon');
    removeFeedback('golive_dates_icon');
    $('#btnCancel').hide();
}

function triggerFeedback(id, type) {
    $('#' + id).parent().closest('div').addClass('has-feedback');
    $('#' + id).parent().closest('div').addClass('has-' + type);
    $('#' + id).show();
}

function removeFeedback(id) {
    $('#' + id).parent().closest('div').removeClass('has-feedback has-success has-warning has-error');
    $('#' + id).hide();
}

var flashMessage = function(data) {
    html = '';
    var $flash = $('#flash');
    numberOfUpdates++;
    if (data.length) {
        for (i = 0; i < data.length; i++) {
            html += '<div class="alert fade in alert-' + data[i]['type'] + '"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data[i].message + ' (' + numberOfUpdates + ')' + '</div>';
        }
    } else {
        html += '<div class="alert fade in alert-' + data['type'] + '"><a href="#" class="close" data-dismiss="alert">&times;</a>' + data.message + ' (' + numberOfUpdates + ')' + '</div>';
    }
    if (numberOfUpdates > 1) {
        $flash.children(0).replaceWith(html);
    } else {
        $flash.append(html);
    }
    clearTimeout(flashMessageTimeout);
    flashMessageTimeout = setTimeout(function() {
        removeFlashMessages($flash.children().eq(-1));
    }, 5000);
    return $flash;
};

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function addFormattingToDynatableOverview(loopCount) {
    console.log('running dynatable formatting. Loop: ' + loopCount);
    var $tasksDynatable = $('#tasks_overview').data('dynatable');
    if($tasksDynatable) {
        var dynatableBody = $('#tasks_overview').data('dynatable').element.children[1];
        $(dynatableBody).find('tr').each(function(index) {
            $(this).find('td').each(function(i) {
                if (i == 0) {
                    var unit_number = $(this).text();
                    var url = '#';
                    var link = unit_number.link(url);
                    $(this).on('click', function() {
                        goSearch(unit_number);
                    });
                    $(this).html(link);
                } else if ($(this).text() == 'Not Started' || $(this).text() == 'Not Ordered') {
                    $(this).css('background-color', '#ff9999');
                } else if ($.inArray($(this).text(), completedStatuses) >= 0) {
                    $(this).css('background-color', 'lightgreen');
                } else if ($(this).text() == "undefined") {
                    $(this).css('background-color', 'crimson');
                } else if ($(this).text() == 'BSM Handling' || $(this).text() == 'Ordered By BSM') {
                    $(this).css('background-color', 'darkgray');
                } else if (i > 1 && i < 9) {
                    $(this).css('background-color', '#ffff4d');
                } else if (i == 11) {
                    var days = /\d+/.exec($(this).text());
                    var daysToGo = $(this).text();
                    if (days <= 14) {
                        $(this).css({'fontWeight': 700, 'color': 'red'});
                    }
                    var goLiveDate = new Date();
                    goLiveDate.setDate(goLiveDate.getDate() + parseInt(days));
                    var dateString = goLiveDate.toLocaleDateString();
                    $(this).prop({'data-toggle': 'tooltip', 'title': dateString});
                    $(this).tooltip();
                } else if (i == 13) {
                    if ($(this).text() == 'New') {
                        $(this).closest('tr').css('opacity', '0.7');
                    }
                }
            });
            if (search_txt != 'None') {
                $tasksDynatable.queries.runSearch(search_txt);
            }
        });
    } else if (loopCount < 20) {
        loopCount++;
        setTimeout(function() {
            addFormattingToDynatableOverview(loopCount)
        }, 500);
    } else if (undefined === loopCount || typeof(loopCount) != 'number') {
        loopCount = 0;
        addFormattingToDynatableOverview();
    }
}

function submitFormData() {
    var $submitButton = $('.btnsubmit');
    if ($submitButton.val().toUpperCase() == 'submit'.toUpperCase()) {
        console.log('submitting new unit data');
        var url = Flask.url_for("add_new_unit");
        var posting = $.post(url, $("#form_unit_info").serialize());
        posting.done(function(data) {
            if (data.length && data[0].type) {
                flashMessage(data).focus();
                // clearForm(false);
                setTimeout(function() {
                    searchFor($('#unit_number').val());
                }, 2000);
            }
        });
    } else if ($submitButton.val().toUpperCase() == 'edit'.toUpperCase()) {
        console.log('activating edit mode');
        disableFields(false);
        $('.noedit').attr('readonly', true);
        return false;
    } else {
        console.log('submitting updated unit data');
        submitInfoToDB();
        return false;
    }
}

function submitToSharepoint() {
    // console.log('posting to sharepoint');
    var url = Flask.url_for("add_to_sharepoint");
    var posting = $.post(url, $("#form_unit_info").serialize());
    posting.done(function( data ) {
        var result = data.result;
        $("#popup_message").empty().append(result).focus();
    });
}

function initSharepointSync() {
    $('.shareStatus').each(function() {
        syncToSharepoint($(this), false);
    })
}

function updateSharepoint(event) {
    syncToSharepoint($(event.target), true);
}

function syncToSharepoint(e, doSubmitToDB) {
    console.log('id: ' + e.prop('id') + ' Value: ' + e.val());
    setSPDropDownsBorderColor('lightgoldenrodyellow');
    var url = Flask.url_for("update_sp");
    var posting = $.post(url,
        {
            unit_number: $('#unit_number').val(),
            field_name: e.prop('id'),
            log_name: e.prop('name'),
            status: e.val(),
            update_log: doSubmitToDB,
            user_name: current_user
        });
    posting.done(function( data ) {
        var result = data.response;
        if (result === true) {
            setSPDropDownsBorderColor('lightgreen');
        } else {
            setSPDropDownsBorderColor('red');
        }
        if (doSubmitToDB) {
            submitInfoToDB(true);
        }
        $(e).parent().find("a[id*='timestamp']").text(new Date().toLocaleString());
    });
}

function goSearch(unit) {
    var currentPage = window.location.pathname;
    if (currentPage != "/" ) {
        sessionStorage.setItem('search_unit', unit);
        window.location.replace("/");
    } else {
        searchFor(unit);
    }
}

function searchFor(unit) {
    if (undefined === unit) {
        var unit = sessionStorage.getItem('search_unit');
        if (!unit) {
            return false;
        } else {
            sessionStorage.removeItem('search_unit');
        }
    }
    if(unit) {
        clearForm(true);
        removeFlashMessages();
        var url = Flask.url_for('unit_lookup');
        $.getJSON(url, {
            lookup_unit: unit
        }, function (data) {
            test_data = data;
            if (data.length && data[0].type) {
                flashMessage(data);
            } else if (data.redirect_url) {
                window.open(data.redirect_url, "_self");
            } else {
                $(document).BindJson(data, 'df_');
                showDeviceRows('#tblPosTerminals tr');
                showDeviceRows('#tblOtherNetworkDevices tr');
                getEmailLinks(unit);
                checkGoLiveDates(unit);
                getLogText(unit);
                //getSharepointData(unit);
                $('.btnsubmit').val('Edit');
            }
        });
    }
    return false;
}

function toggleSameShipping() {
    if ($('#same_shipping_address').prop('checked')) {
        $('#service_address > .panel-body > .form-group').children().each(function (index) {
            $(this).children('input,select').each(function () {
                var shippingAddressChildren = $('#shipping_address > .panel-body > .form-group').children().children('input,select').eq(index);
                shippingAddressChildren.val($(this).val());
                shippingAddressChildren.attr('readonly', true);
            });
        });
    } else {
        $('#shipping_address > .panel-body > .form-group').children().each(function () {
            $(this).children('input,select').val('');
            $(this).children('input,select').attr('readonly', false);
        });
    }
}

function getEmailLinks(unit) {
    $('#emails').empty();
    var url = Flask.url_for("get_email_links");
    $.getJSON(url, {
        unit_number: unit
    }, function (data) {
        data.result.forEach(function(item, index, arr)  {
            var email_timestamp = document.createElement("Label");
            var time_text = document.createTextNode(item.time.toString());
            email_timestamp.appendChild(time_text);
            email_timestamp.style = 'padding-right: 10px';
            var email_link = document.createElement("A");
            var link_text = document.createTextNode(item.title.toString());
            email_link.appendChild(link_text);
            email_link.href = $SCRIPT_ROOT + '/email/' + item.content.toString();
            email_link.id = "email_" + index;
            var email_item = document.createElement('li');
            email_item.className = 'list-group-item';
            email_item.appendChild(email_timestamp);
            email_item.appendChild(email_link);
            //$('#emails').append("<br>")
            $('#emails').append(email_item);
        });
    });
}

function getLogText(unit) {
    var url = Flask.url_for('get_status_log');
    $.getJSON( url, {
        unit_number: unit
    }, function(data) {
        $('#status_log').val(data.result);
    })
}

function checkGoLiveDates(unit) {
    var url = Flask.url_for('get_golive_dates');
    var $label = $('#golive_date').parent().parent().find('label');
    $label.popover('destroy');
    $.getJSON( url, {
        unit_number: unit
    }, function(data) {
        if (!$.isEmptyObject(data.dates) && !data.dates.hasOwnProperty('error')) {
            triggerFeedback('golive_dates_icon', 'warning');
            //var date_text = '';
            var $content_div = $(document.createElement('div'));
            for (var key in data.dates) {
                var dt = new Date(data.dates[key]);
                dt.setDate(dt.getUTCDate());
                var date = dt.toLocaleDateString();
                var $row_div = $(document.createElement('div'));
                var task_link = document.createElement('A');
                var link_text = document.createTextNode(key);
                var date_text = document.createTextNode(' - ' + date);
                task_link.appendChild(link_text);
                task_link.href = 'https://remedyweb.compass-usa.com/arsys/servlet/ViewFormServlet?form=FSS%3aTask&server=remedyprod.NA.CompassGroup.Corp&eid=' + key;
                task_link.target = '_blank';
                $row_div.append([task_link, date_text]);
                $content_div.append($row_div);
            }
            $label.popover({content: $content_div, html: true});
        } else if (data.dates.hasOwnProperty('error')) {
            triggerFeedback('golive_dates_icon', 'error');
        } else {
            triggerFeedback('golive_dates_icon', 'success');
        }
    })
}

function showDeviceRows(deviceTable) {
    $(deviceTable).each(function() {
        $('td', this).each(function() {
            if ($(this).children(0).val() != '') {
                $(this).closest('tr').show();
            }
        });
    });
}

function showNextRow(event) {
    var row = event.target.closest("tr");
    $(row).next().show();
}

function removeCurrentRow(event) {
    var row = event.target.closest("tr");
    $(row).ClearDataFields('clearMe');
    $(row).hide();
}

function getSharepointData(unit) {
    setSPDropDownsBorderColor('lightgoldenrodyellow');
    $.getJSON($SCRIPT_ROOT + '/sp_unit_lookup', {
        unit_number: unit
    }, function(data) {
        if (data.result == false || data.unit_number == 0) {
            var result = 'Unable to locate unit in Sharepoint';
            setSPDropDownsBorderColor('yellow');
        } else if (data.errors) {
            var result = data.errors;
            setSPDropDownsBorderColor('red');
        } else {
            $("[name='bb_order_status']").val(data.Order_x0020_Broadband);
            $("[name='router_install_status']").val(data.Router_x0020_Installation);
            $("[name='network_diagram_status']").val(data.Network_x0020_Diagram);
            $("[name='router_order_status']").val(data.Order_x0020_Router_x002f_Switch);
            $("[name='merchantlink_status']").val(data.Merchant_x0020_Link_x0020_Setup);
            $("[name='cc_boarding_status']").val(data.FP_x0020_Boarding);
            $("[name='cc_order_status']").val(data.P2PE_x0020_Device_x0020_Status);
            $("[name='mid_request_status']").val(data.MID);
            $("[name='project_status']").val(data.Project_x0020_Status);
            setSPDropDownsBorderColor('lightgreen');
        }
        $("#popup_message").empty().append(result);
    });
}

function searchForUnit(unit) {
    searchDBForUnit(unit);
    //searchSharepointForUnit(unit);
}

function searchDBForUnit(unit) {
    $.getJSON($SCRIPT_ROOT + '/_unit_exists', {
        unit_number: unit
    }, function(data) {
        if (data.result) {
            $('#warningModal').modal();
        }
        // $('#unit_name').val(data.result);
    });
}

function searchSharepointForUnit(unit) {
    $.getJSON($SCRIPT_ROOT + '/sp_unit_lookup', {
        unit_number: unit
    }, function(data) {
        if (data.errors) {
            console.log(data.errors);
            return data.errors;
        } else if (data.unit_number == 0) {
            //$('#popup_message').text('Not in Sharepoint');
            $('#sp_check_icon').hide();
            unitIsOnSharepoint = false;
        } else {
            triggerFeedback('sp_check_icon', 'success');
            unitIsOnSharepoint = true;
        }
    });
}

function getOrderPDFDownload() {
    var unit_num = $('#unit_number').val();
    document.location.href=Flask.url_for('generate_order_pdf', {'unit_number': unit_num});
}

function getBoardingPDFDownload() {
    var unit_num = $('#unit_number').val();
    document.location.href=Flask.url_for('generate_switching_pdf', {'unit_number': unit_num})
}

function removeFlashMessages(message) {
    numberOfUpdates = 0;
    if (message) {
        message.alert('close');
    } else {
        $('#flash').children().each(function () {
            $(this).alert('close');
        });
    }
}

function clearForm(disableStatus) {
    $(document).ClearDataFields('clearMe');
    $('.shareStatus').each(function() {
        $(this).val('NA');
        setSPDropDownsBorderColor('white');
    });
    disableFields(disableStatus);
    $('#add_unit_question').hide();
    hideFields();
}

function cancelEditMode() {
    disableFields(true);
    $('.btnsubmit').val('Edit');
}

function disableFields(status) {
    $(".disableInput").attr("disabled", status);
    var $submitVal = $('.btnsubmit').val();
    console.log('btnsubmit val: ' + $submitVal);
    if (status && $submitVal != 'Submit') {
        $('.btnsubmit').val('Edit');
        $('#btnCancel').hide();
    } else if ($submitVal != 'Submit') {
        $('.btnsubmit').val('Update');
        $('#btnCancel').show();
    }
}

function setSPDropDownsBorderColor(color) {
    $('.shareStatus').each(function() {
        $(this).css('border-width', 'initial');
        $(this).css('border-color', color);
    });
}

function submitInfoToDB(retainStatus) {
    if (retainStatus && $('.btnsubmit').val().toUpperCase() == 'EDIT') {
        var disabled = true;
        disableFields(false);
    }
    var url = Flask.url_for("update_entry");
    var posting = $.post(url, $("#form_unit_info").serialize());
    posting.done(function (data) {
        flashMessage(data);
        if (retainStatus) {
            $('#flash').focus();
        }
    });
    if (retainStatus && disabled) {
        disableFields(true);
    } else if (!retainStatus) {
        disableFields(true);
    }
}

function openRemedyTask(e, baseURL) {
    var ticketNumber = $('#' + e.prop('for')).val();
    if (!baseURL) {
        var baseURL = 'https://remedyweb.compass-usa.com/arsys/servlet/ViewFormServlet?form=FSS%3aTask&server=remedyprod.NA.CompassGroup.Corp&eid=';
    }
    var url = baseURL + ticketNumber;
    console.log('attempting to open url: ' + url);
    window.open(url,'_blank');
}

function openCompassNETTask(e) {
    var baseURL = 'https://remedyweb.compass-usa.com/arsys/servlet/ViewFormServlet?form=OM%3AOrders&server=remedyprod.NA.CompassGroup.Corp&eid=';
    openRemedyTask(e, baseURL);
}

function openTreasuryTask(e) {
    var baseURL = 'https://remedyweb.compass-usa.com/arsys/servlet/ViewFormServlet?form=TREAS%3ARequest&server=remedyprod.NA.CompassGroup.Corp&eid=';
    openRemedyTask(e, baseURL);
}

function mailTo(e) {
    var emailAddress = $('#' + e.prop('for')).val();
    var url = 'mailto:' + emailAddress;
    var myWin = window.open(url);
    setTimeout(function() {
        myWin.close();
    }, 500);
}

function editButtonHandler(event) {
    var e = $(event.target);
    if (e.val().toUpperCase() == 'EDIT') {
        disableFields(false);
        return false;
    } else {
        submitInfoToDB();
        return false;
    }
}

Mousetrap.bind(['ctrl+e', 'ctrl+s', 'alt+e', 'alt+s'], function(e) {
    submitFormData();
    return false;
});

function rebuildParameterString(params) {
  var paramString = '?';
  $.each(params, function(index, value) {
    paramString += value;
    if (index < params.length - 1) {
      paramString += '&';
    }
  });
  return paramString;
}
