/**
 * Created by wisdomwolf on 6/18/2016.
 */

$(document).ready(function() {
    console.log('ready');
    if ($('#unit_number').val()) {
        //Page is dirty, trigger search to populate data correctly
        goSearch($('#unit_number').val());
    }
    $('#btnAddYes').on('click', function() {
        submitToSharepoint();
    });
    hideFields();
    $("button.shownextrow").on('click', showNextRow);
    $("button.removethisrow").on('click', removeCurrentRow);
    $('.shareStatus').each(function() {
        $(this).on('change', updateSharepoint);
    });
    $("#same_shipping_address").click(toggleSameShipping);
    $('#service_address > .panel-body > .form-group').children().children('input,select').each(function (index) {
        if ($(this).id != '') {
            $(this).on('change', function () {
                if ($('#same_shipping_address').prop('checked')) {
                    $('#shipping_address > .panel-body > .form-group').children().children('input,select').eq(index).val($(this).val());
                }
            });
        }
    });
    $("#same_shipping_address").on('focusout', function() {
        if ($("#same_shipping_address").prop("checked")){
            $('#contact_name').focus();
        }
    });

    $('#unit_number').on('focusout', function() {
        searchForUnit($('#unit_number').val());
    });

    $('#unit_number').on('focusin', function() {
        $('#sp_check_icon').hide();
    });
    $("[name*='phone'], [name='landline']").inputmask({ mask: "999-999-9999[x9{0,4}]", skipOptionalPartCharacter: " ", greedy: false });
    $("[name*='email']").inputmask("email");
    $(".ip_address").inputmask("9{1,3}.9{1,3}.9{1,3}.9{1,3}");
    $('.remedy_task, .remedy_treasury_order, .remedy_compassnet_order, .email_label').each(function() {
        var label_text = $(this).text();
        var url = '#';
        var link = label_text.link(url);
        $(this).html(link);
        $(this).find('a').prop('tabindex', '-1');
    });
    $('.remedy_task').click(function(){ openRemedyTask($(this)); return false; });
    $('.remedy_treasury_order').click(function(){ openTreasuryTask($(this)); return false; });
    $('.remedy_compassnet_order').click(function(){ openCompassNETTask($(this)); return false; });
    $('.email_label').click(function() { mailTo($(this)); return false; });
    $('#btnViewExistingUnit').click(function() {
        searchFor($('#unit_number').val());
        $('#warningModal').modal('hide');
    });
    setTimeout(goSearch(), 100);

    $.fn.responsiveTabs = function() {
        this.addClass('responsive-tabs');
        this.append($('<span class="glyphicon glyphicon-triangle-bottom"></span>'));
        this.append($('<span class="glyphicon glyphicon-triangle-top"></span>'));

        this.on('click', 'li.active > a, span.glyphicon', function() {
            this.toggleClass('open');
        }.bind(this));

        this.on('click', 'li:not(.active) > a', function() {
            this.removeClass('open');
        }.bind(this));
    };
    $('.well').css("z-index", '9999');
    $('.nav.nav-tabs').responsiveTabs();
    $('#warningModal').on('hidden.bs.modal', function () {
        $('.well').css("z-index", '9999');
    });
    $('#warningModal').on('show.bs.modal', function () {
        $('.well').css("z-index", '-1');
    });
    // http://stackoverflow.com/questions/25855428/make-bootstrap-3-tabs-responsive
});