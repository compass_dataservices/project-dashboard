/**
 * Created by wisdomwolf on 6/18/2016.
 */
$(document).ready(function() {
    var url;
    $(function () {
        if (search_txt == "None" && historic == "None") {
            url = Flask.url_for('all_current_units_json');
        } else {
            url = Flask.url_for('all_units_json');
        }
        $.ajax({
            url: url,
            success: function (data) {
                $('#tasks_overview').dynatable({
                    table: {
                        defaultColumnIdStyle: 'underscore'
                    },
                    dataset: {
                        records: data
                    },
                    features: {
                        paginate: true,
                        recordCount: true
                    },
                    readers: {
                        golive_date: function(cell, record) {

                            var $cell = $(cell),
                                dec = /\d+/.exec($cell.text());

                            record['dec'] = dec;
                            console.log($cell.text() + ' = ' + record['dec']);

                            // Return the HTML of the cell to be stored
                            // as the "days" attribute.
                            return $cell.html();
                        }
                    },
                    inputs: {
                        queries: $('#assigned-tpa, #assigned-bsm')
                    }
                }).on('dynatable:afterUpdate', addFormattingToDynatableOverview).on('dynatable:push', function() {
                    setTimeout(function() {
                        var overviewParameters = window.location.search.replace('?', '').split('&');
                        console.log('storing parameters: ' + overviewParameters);
                        sessionStorage.setItem('overviewParameters', overviewParameters);
                    }, 500);
                });
            }
        });
        addFormattingToDynatableOverview(0);
    });
    if (sessionStorage.getItem('overviewParameters')) {
        var overviewParameters = sessionStorage.getItem('overviewParameters').split(',');
        sessionStorage.removeItem('overviewParameters');
        var redirect_url = window.location.pathname + rebuildParameterString(overviewParameters);
        console.log('redirecting to ' + redirect_url);
        //window.location = redirect_url;
    }
    $('#include_historic').click(function() {
        var $historic = $('#include_historic')[0].checked;
        var redirect_url = Flask.url_for('show_overview_dashboard') + '?';
        if ($historic) {
            console.log('including historic data');
            redirect_url += 'historic=1&'
        } else {
            console.log('loading current data');
        }
        redirect_url += 'queries[' + user_role + ']=' + user_first_name + '+' + user_last_name + '&sorts[unit_name]=1';
        window.open(redirect_url, "_self");
    });
});