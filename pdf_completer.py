#!python3

import csv
from fdfgen import forge_fdf
import os
import sys
import pdb
from utils import *

from datetime import datetime, timedelta

sys.path.insert(0, os.getcwd())
try:
    STATIC_FILES = os.path.join(os.getenv('FLASK_STATIC'), 'files')
except TypeError:
    print('unable to read environment variable: {}'.format('FLASK_STATIC'))
    STATIC_FILES = os.path.join(os.getcwd(), 'static', 'files')

DEVICE_ORDER_SUFFIX = "FreedomPay_Device_Order_Form.pdf"
DEVICE_ORDER_PDF = os.path.join(STATIC_FILES, DEVICE_ORDER_SUFFIX)
FP_SWITCHING_SUFFIX = "P2PE_Switching_Form.pdf"
FP_SWITCHING_PDF = os.path.join(STATIC_FILES, FP_SWITCHING_SUFFIX)
TMP_FILE = "tmp.fdf"
try:
    OUTPUT_FOLDER = os.getenv('FLASK_TEMP')
except TypeError:
    print('unable to read environment variable: {}'.format('FLASK_TEMP'))
    OUTPUT_FOLDER = os.path.join(os.getcwd(), '.temp')

AG_VERSION = '4.4.8'
AG_WORKSTATION = 'J2'
MC_VERSION = 'Simphony 2.7 MR4 HF4'
MC_WORKSTATION = 'Workstation 6'

for file in os.listdir(OUTPUT_FOLDER):
    os.remove(os.path.join(OUTPUT_FOLDER, file))

device_order_fields = ["Date Submitted", "Requested Delivery Date",
                       "Requested Install Date", "PartnerMaster Account",
                       "Sector", "Site Name", "Unit Number",
                       "Site Shipping Address please include special "
                       "shipping instructions where applicable",
                       "Contact Name", "Email Address", "Phone Number",
                       "BSM Contact Name", "Email Address_2", "PID",
                       "Manager Name", "Manager Email", "POS Type",
                       "Version", "Workstation Types", "Quantity379",
                       "US or Canada_379", "Total379", "Quantity349",
                       "US or Canada349", "Total349", "Quantity879",
                       "US or Canada879", "Total879", "Quantity75",
                       "US or Canada75", "Total75", "Quantity25",
                       "US or Canada25", "Total25", "TotalTOTAL AMOUNT",
                       "NotesRow1", "Cost_1", "Cost_2", "Cost_3",
                       "Cost_4", "Cost_5"
                       ]

unused_fields = ["Quantity879", "US or Canada879",
                 "Total879", "Quantity25", "US or Canada25",
                 "Total25", "NotesRow1", "Cost_1", "Cost_2", "Cost_3",
                 "Cost_4", "Cost_5"]

shipping_address_key = "Site Shipping Address please include special " \
                       "shipping instructions where applicable"


def create_order_fields_from_object(json, db):
    #TODO Adjust devices for rGuest kiosks
    date_submitted = datetime.now().strftime('%m/%d/%Y')
    golive_datetime = json.get('golive_date')
    requested_delivery_date = (golive_datetime - timedelta(weeks=2)).strftime('%m/%d/%Y')
    requested_install_date = golive_datetime.strftime('%m/%d/%Y')
    master_account = json.get('master_account', json['unit_name'])
    sector = json.get('sector', json.get('unit_sector', ''))
    site_name = json['unit_name']
    unit_number = json['unit_number']
    try:
        shipping_address = '{street};{city}, {state};{zip}'.format(**json['shipping_address'])
    except KeyError:
        shipping_address = 'ERROR: Enter Manually'
    contact_name = json.get('unit_contact').get('name', 'TBD')
    unit_email = json.get('unit_contact').get('email', '')
    unit_phone = json.get('unit_contact').get('phone', '000-000-0000')
    bsm_contact = json.get('solution_manager')
    bsm_email = db.fss_contacts.find_one({'name': bsm_contact}).get('email')
    tpa_contact = json.get('project_analyst')
    if tpa_contact == 'TBD' or tpa_contact == 'None':
        tpa_contact = bsm_contact
    tpa_email = db.fss_contacts.find_one({'name': tpa_contact}).get('email')
    remedy_number = json.get('payment_task', 'XXXXX')
    pid = 'BSM{}{}'.format(get_initials(tpa_contact), remedy_number[-5:])
    manager_name = json.get('unit_contact').get('name', 'TBD')
    manager_email = json.get('unit_contact').get('email', '')
    try:
        pos_type = 'Agilysys' if 'ysys' in json.get('network_devices').get('pos_terms')[0]['type'] or\
                                 'infogenesis' == json.get('network_devices').get('pos_terms')[0]['type'] else 'Micros'
    except IndexError:
        pos_type = 'TBD'
    version = AG_VERSION if pos_type == 'Agilysys' else MC_VERSION
    workstation_type = AG_WORKSTATION if pos_type == 'Agilysys' else MC_WORKSTATION
    quantity_ipp350_usb = int(json.get('cc_devices').get('qty', 0))
    total_ipp350_usb = quantity_ipp350_usb * 369.00
    quantity_ipp320 = 0
    total_ipp320 = quantity_ipp320 * 349.00
    quantity_75 = quantity_ipp350_usb + quantity_ipp320
    total_75 = quantity_75 * 75.00
    total_amount = total_75 + total_ipp320 + total_ipp350_usb

    IPP350_USB = 'Quantity76'
    IPP350_ETH = 'Quantity77'
    IPP320_USB = 'Quantity78'
    IWL252_WLS = 'Quantity75'
    HONEYWELL  = 'Quantity26'


    fields = [
        ("Date Submitted", date_submitted),
        ("Requested Delivery Date", requested_delivery_date),
        ("Requested Install Date", requested_install_date),
        ("PartnerMaster Account", master_account),
        ("Sector", sector),
        ("Site Name", site_name),
        ("Unit Number", unit_number),
        (shipping_address_key, shipping_address),
        ("Contact Name", contact_name),
        ("Email Address", '{}; {}'.format(unit_email, tpa_email)),
        ("Phone Number", unit_phone),
        ("BSM Contact Name", bsm_contact),
        ("Email Address_2", bsm_email),
        ("PID", pid),
        ("Manager Name", manager_name),
        ("Manager Email", manager_email),
        ("POS Type", pos_type),
        ("Version2", version),
        ("Workstation Types", workstation_type),
        ("Quantity379", quantity_ipp350_usb),
        ("Total379", '${:.2f}'.format(total_ipp350_usb)),
        ("Quantity349", quantity_ipp320),
        ("Total349", '${:.2f}'.format(total_ipp320)),
        ("Quantity75", quantity_75),
        ("Total75", '${:.2f}'.format(total_75)),
        ("TotalTOTAL AMOUNT", '${:.2f}'.format(total_amount))
    ]
    return fields


def create_switch_fields_from_object(json, db):
    business_name = json['unit_name']
    unit_number = json.get('unit_number')
    submitter = json['project_analyst']
    if submitter == 'TBD' or submitter == 'None':
        submitter = json['solution_manager']
    contact_name = json.get('unit_contact').get('name')
    company = 'Compass Group'
    unit_email = json.get('unit_contact').get('email')
    submitter_phone = [x for x in db.fss_contacts.find({'name': submitter})][0].get('phone')
    submitter_email = list(db.fss_contacts.find({'name': submitter}))[0].get('email')
    unit_phone = json.get('unit_contact').get('phone')
    unit_address = json.get('service_address').get('street')
    target_install_date = generate_date_string(json.get('golive_date'))
    unit_city = json.get('service_address').get('city')
    unit_state = json.get('service_address').get('state')
    unit_zip = json.get('service_address').get('zip')
    mids = 1
    dropdown3 = 'First Data'
    platform = 'Nashville'
    tax_id = str(os.getenv('TAX_ID'))
    number_of_workstations = json.get('cc_devices').get('qty')
    unit_contact_first_name = json.get('unit_contact').get('name').split(' ')[0]
    unit_contact_last_name = json.get('unit_contact').get('name').split(' ')[1]
    try:
        pos_type = 'Agilysys' if 'ysys' in json.get('network_devices').get('pos_terms')[0]['type'] or \
                                 'infogenesis' == json.get('network_devices').get('pos_terms')[0]['type'] else 'Micros'
    except IndexError:
        pos_type = 'TBD'

    fields = [
        ("Business Name", business_name),
        ("Submitted By", submitter),
        ("Contact Name", contact_name),
        ("Company", company),
        ("Email Address", unit_email),
        ("Phone Number", submitter_phone),
        ("Office Phone", unit_phone),
        ("Email Address_2", submitter_email),
        ("Street Address", unit_address),
        ("Target Install Date", target_install_date),
        ("City", unit_city),
        ("State", unit_state),
        ("Zip", unit_zip),
        ("MIDs", mids),
        ("Dropdown3", dropdown3),
        ("Platform", platform),
        ("VisaC", 'Yes'),
        ("JCBC", 'No'),
        ("TaxID", tax_id),
        ("AMEXC", 'Yes'),
        ("UnionPayC", 'No'),
        ("Dropdown1", '5:00AM'),
        ("MastercardC", 'Yes'),
        ("WithTipsC", 'No'),
        ("DiscoverC", 'Yes'),
        ("Number of Workstations", number_of_workstations),
        ("First Name", unit_contact_first_name),
        ("Last Name", unit_contact_last_name),
        ("Email Address_3", unit_email),
        ("Type", pos_type),
        ("RVCProfit Center Name RVCProfit Center Number Processor MID Processor TID To Be Completed By ClientRow1", business_name),
        ("RVCProfit Center Name RVCProfit Center Number Processor MID Processor TID To Be Completed By ClientRow1_2", unit_number)
    ]

    return fields


def form_fill(fields, pdf_file, unit_number, unit_name):
    fdf = forge_fdf("", fields, [], [], [])
    try:
        fdf_file = open(TMP_FILE, "w")
        fdf_file.write(fdf)
    except TypeError:
        print('decoding file due to type error')
        try:
            fdf_w = fdf.decode('latin-1')
            fdf_file.write(fdf_w)
        except UnicodeEncodeError:
            try:
                fdf_file = open(TMP_FILE, "wb")
                fdf_file.write(fdf)
            except TypeError:
                fdf = fdf.decode('utf8')
                fdf_file.write(fdf)
    fdf_file.close()
    filename_prefix = os.path.splitext(os.path.basename(pdf_file))[0]
    if not os.path.exists(OUTPUT_FOLDER):
        os.mkdir(OUTPUT_FOLDER)
    output_file = '{0}/{2}_{3}_{1}.pdf'.format(OUTPUT_FOLDER, filename_prefix, unit_number, unit_name)
    cmd = 'pdftk "{0}" fill_form "{1}" output "{2}" dont_ask'.format(pdf_file, TMP_FILE, output_file)
    print('Running command: {}'.format(cmd))
    os.system(cmd)
    os.remove(TMP_FILE)


def generate_order_form(unit_number, db):
    unit = db.unit_info.find_one({'unit_number': str(unit_number)})
    if unit:
        data = create_order_fields_from_object(unit, db)
        form_fill(data, DEVICE_ORDER_PDF, data[6][1], data[5][1])
        return True
    else:
        return False


def generate_switching_form(unit_number, db):
    unit = db.unit_info.find_one({'unit_number': str(unit_number)})
    if unit:
        data = create_switch_fields_from_object(unit, db)
        form_fill(data, FP_SWITCHING_PDF, unit_number, data[0][1])
        return True
    else:
        return False
